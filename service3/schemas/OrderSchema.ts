import {
  Schema,
} from "mongoose";

const OrderSchema = new Schema({
  numOrders: {
    required: true,
    type: Number,
  },
  price: {
    required: true,
    type: Number,
  },
  size: {
    required: true,
    type: Number,
  },
});

export default OrderSchema;
