import {
  model,
  Schema,
} from "mongoose";
import OrderSchema from "./OrderSchema";

const schema = new Schema({
  asks: {
    default: [],
    required: true,
    type: [OrderSchema],
  },
  at: {
    default: Date.now,
    required: true,
    type: Date,
  },
  bids: {
    default: [],
    required: true,
    type: [OrderSchema],
  },
  sequence: {
    required: true,
    type: Number,
  },
});

export const OrderBookSchema = model("OrderBook", schema);
