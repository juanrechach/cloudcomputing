import * as mongoose from "mongoose";
import * as express from "express";
import * as http from "http";
import * as cors from "cors";

import { OrderBookSchema } from "./schemas/OrderBookSchema";

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();

app.use(cors());

(() => {
  mongoose.connect("mongodb://mongo:27017");
  (mongoose as any).Promise = global.Promise;
})();

app.get('/', async (_, res) => {
  console.log("Requesting data from Database");
  const data = await OrderBookSchema.find();
  res.json(data[data.length -1]);
});

const server = http.createServer(app);

server.listen(PORT);

console.log(`Running on http://${HOST}:${PORT}`);
