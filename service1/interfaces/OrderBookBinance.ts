export interface OrderBookBinance {
  lastUpdateId: number;
  bids: string[][];
  asks: string[][];
}
