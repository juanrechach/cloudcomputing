import { Order } from "./Order";

export interface OrderBook {
  at?: Date;
  asks: Order[];
  bids: Order[];
  sequence: number;
}
