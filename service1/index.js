"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const OrderBookSchema_1 = require("./schemas/OrderBookSchema");
const request_1 = require("./utils/request");
// Constants
const ENDPOINT = "https://api.binance.com/api/v1/";
(() => {
    // mongoose.connect("mongodb://admin:abc123!@localhost/InfoDatabase");
    mongoose.connect("mongodb://mongo:27017");
    mongoose.Promise = global.Promise;
})();
const requestOrderBooks = function () {
    return __awaiter(this, void 0, void 0, function* () {
        const ordersBookStr = yield request_1.default.get(`${ENDPOINT}/depth`, {
            headers: {
                "Content-Type": "application/json",
            },
            qs: {
                limit: 10,
                symbol: "ETHUSDT",
            },
        });
        return JSON.parse(ordersBookStr);
    });
}(() => {
    getOrderBookAndStoreToMongoDB();
    setInterval(getOrderBookAndStoreToMongoDB, 10000);
})();
function getOrderBookAndStoreToMongoDB() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const date = new Date();
            console.log(`Requesting OrderBook from GDAX at ${date}`);
            const data = yield requestOrderBooks();
            data.at = date;
            console.log("Saving data to Database");
            yield OrderBookSchema_1.OrderBookSchema.create(data);
            console.log("created");
        }
        catch (error) {
            console.log("Error" + error);
        }
    });
}
//# sourceMappingURL=index.js.map