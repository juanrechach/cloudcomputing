import * as mongoose from "mongoose";

import { OrderBookSchema } from "./schemas/OrderBookSchema";
import request from "./utils/request";
import { OrderBookBinance } from "./interfaces/OrderBookBinance";

// Constants
const ENDPOINT = "https://api.binance.com/api/v1/";

(() => {
  // mongoose.connect("mongodb://admin:abc123!@localhost/InfoDatabase");

  mongoose.connect("mongodb://mongo:27017");
  (mongoose as any).Promise = global.Promise;
})();

async function requestOrderBooks() {
  const ordersBookStr = await request.get<string>(`${ENDPOINT}/depth`,
    {
      headers: {
        "Content-Type": "application/json",
      },
      qs: {
        limit: 10,
        symbol: "ETHUSDT",
      },
    });
  return JSON.parse(ordersBookStr) as OrderBookBinance;
}

(() => {
  getOrderBookAndStoreToMongoDB();
  setInterval(getOrderBookAndStoreToMongoDB, 10000);
})()

async function getOrderBookAndStoreToMongoDB() {

  try {
    const date = new Date();

    console.log(`Requesting OrderBook from GDAX at ${date}`);

    const data: OrderBookBinance = await requestOrderBooks();
    
    console.log("Saving data to Database");

    await OrderBookSchema.create(data);

    console.log("created");

  } catch (error) {
    console.log("Error" + error);
  }
}
