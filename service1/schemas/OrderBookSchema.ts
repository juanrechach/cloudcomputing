import {
  model,
  Schema,
} from "mongoose";

const schema = new Schema({
  asks: {
    default: 0,
    required: true,
    type: [[String]],
  },
  bids: {
    default: 0,
    required: true,
    type: [[String]],
  },
  lastUpdateId: {
    required: true,
    type: Number,
  },
});

export const OrderBookSchema = model("OrderBook", schema);
