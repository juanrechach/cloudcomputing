import * as request from "request";

function get<T>(uri: string, options: request.CoreOptions): Promise<T> {
  return new Promise<T>((resolve, reject) => {
    request.get(uri, options, (error: any, _: request.RequestResponse, body: any) => {
      if (error) {
        reject();
      } else {
        resolve(body);
      }
    });
  });
}

export default {
  get,
};
